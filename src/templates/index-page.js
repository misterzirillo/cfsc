import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import Layout from '../components/layout/Layout'
import Nav from '../components/nav/Nav';

import style from './index-page.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const IndexPageTemplate = ({
  image,
  // title,
  heading,
  subheading,
}) => {
  const heroUrl = image.childImageSharp ? image.childImageSharp.fluid.src : image
  const heroImgSrc = {
    backgroundImage: `url(${heroUrl})`
  }

  return (
    <div>
      <section className={`hero is-fullheight is-link ${style.heroImg}`} style={heroImgSrc}>
        <div className="hero-head">
          <Nav />
        </div>
        <div className="hero-body">
          <div className="container">
            <h1 className="title is-family-secondary is-size-1">
              {heading}
            </h1>
            <h2 className="subtitle is-size-2">
              {subheading}
            </h2>
            <a className="button is-large is-outlined">
              Submit
            </a>
          </div>
        </div>
      </section>
    </div>
  )
}

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string,
}

const IndexPage = ({ data }) => {
  const {
    image,
    title,
    heading,
    subheading,
  } = data.markdownRemark && data.markdownRemark.frontmatter

  return (
    <Layout noNav>
      <IndexPageTemplate
        image={image}
        title={title}
        heading={heading}
        subheading={subheading}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.shape({
        title: PropTypes.string,
        heading: PropTypes.string,
        subheading: PropTypes.string,
        image: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.object,
        ]),
      }),
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: {templateKey: {eq: "index-page" } }) {
      frontmatter {
        title
        heading
        subheading
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
