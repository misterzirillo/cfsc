---
templateKey: index-page
title: Canyon Ferry Sailing School 
heading: Big Sky. Big Wind.
subheading: Sailing opportunities on Canyon Ferry Lake
image: /img/hero-2.jpeg

texts:
  - text one
  - text two
  - text three
  - text four
---
