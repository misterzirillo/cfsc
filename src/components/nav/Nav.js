import React, { useState } from 'react'
// import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Burger from './Burger'
import css from './Nav.module.scss'
import logo from '../../img/logo.svg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Nav = () => {
  const [active, setActive] = useState(false)
  const activeClass = active ? 'is-active' : ''

  return (
    <nav
      className="navbar is-link"
      role="navigation"
      aria-label="main-navigation"
    >
      <div className="container">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item" title="Logo">
            <img className={css.logo} src={logo} alt="Kaldi" />
          </Link>
          <Burger active={active} dataTarget="navMenu" onClick={() => setActive(!active)} />
        </div>
        <div
          id="navMenu"
          className={`navbar-menu ${activeClass}`}
        >
          <div className="navbar-end has-text-centered">
            <Link className="navbar-item is-size-6" activeClassName="is-active" to="/lessons">
              Lessons
            </Link>
            <Link className="navbar-item his-size-6" activeClassName="is-active" to="/charters">
              Charters
            </Link>
            <Link className="navbar-item his-size-6" activeClassName="is-active" to="/events">
              Events
            </Link>
            <Link className="navbar-item his-size-6" activeClassName="is-active" to="/about">
              About
            </Link>
            <Link className="navbar-item his-size-6" activeClassName="is-active" to="/contact">
              Contact
            </Link>
          </div>
          <div className="navbar-end has-text-centered">
            <a
              className="navbar-item"
              href="https://github.com/netlify-templates/gatsby-starter-netlify-cms"
              target="_blank"
              rel="noopener noreferrer"
            >
              <span className="icon">
                <FontAwesomeIcon icon={['fab', 'github']} />
              </span>
            </a>
          </div>
        </div>
      </div>
    </nav>
  )
}

Nav.propTypes = {}

export default Nav
