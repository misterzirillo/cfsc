import React from 'react'
import PropTypes from 'prop-types'

const Burger = ({ active, dataTarget, onClick }) => {
  const activeClass = active && 'is-active'
  return (
    <a
      role="button"
      className={`navbar-burger burger ${activeClass}`}
      data-target={dataTarget}
      onClick={onClick}
    >
      <span aria-hidden="true" />
      <span aria-hidden="true" />
      <span aria-hidden="true" />
    </a>
  )
}

Burger.propTypes = {
  onClick: PropTypes.func,
  active: PropTypes.bool,
  dataTarget: PropTypes.string
}

export default React.memo(Burger)
