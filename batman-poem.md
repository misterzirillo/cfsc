

    I would like to believe
    that there exists
    a man consumed
    by an obsession

    to exact a price from that which took
    his humanity

    A lost soul amongst shadows.
    A demon amongst men.
    A bane to all that is evil.

    “Hello again. Beware… forever.”
